### About

Localcfg is a help for customising Vim configuration based on hostname, domainname, features compiled into Vim, and pretty much anything else you can come up with a test for.

### Installing

Use [vundle](https://github.com/gmarik/Vundle.vim) to install it:

    Bundle 'https://gitlab.com/magus/localcfg.git'

Then read the included documentation.
