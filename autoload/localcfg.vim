" Load once only
if exists("g:loaded_localcfg") || &cp
    finish
endif
let g:loaded_localcfg = 'v0'

if !exists("g:localcfg_cfgs")
    let g:localcfg_cfgs = []
endif

if !exists("g:localcfg_features")
    let g:localcfg_features = []
endif

if !exists("g:localcfg_dir")
    let g:localcfg_dir = '~/.vim/localcfg/'
endif

fun! s:create_fn_array()
    let l:fn_array = {}
    let l:fn_list = split(glob(g:localcfg_dir . '*.vim'), '\n')

    for full_fn in l:fn_list
        let l:fn_parts = split(fnamemodify(full_fn, ':t:r'), '-')
        let l:fn_array[full_fn] = l:fn_parts
    endfor

    return l:fn_array
endfun

fun! s:elem(lst, val)
    return index(a:lst, a:val) != -1
endfun

fun! s:subset(lst1, lst2)
    let l:res = 1
    for item in a:lst1
        let l:res = l:res && s:elem(a:lst2, item)
    endfor
    return l:res
endfun

fun! localcfg#get_effective_list()
    let l:localcfg_cfgs = copy(g:localcfg_cfgs)

    " add host and domain
    let l:localcfg_cfgs = add(l:localcfg_cfgs, hostname())
    let l:localcfg_cfgs = extend(l:localcfg_cfgs, split(system('hostname --domain'), '\n'))
    let l:localcfg_cfgs = extend(l:localcfg_cfgs, split(system('hostname --long'), '\n'))

    " add features
    for feat in g:localcfg_features
        if has(feat)
            let l:localcfg_cfgs = add(l:localcfg_cfgs, feat)
        else
            let l:localcfg_cfgs = add(l:localcfg_cfgs, 'not' . feat)
        endif
    endfor

    return l:localcfg_cfgs
endfun

fun! localcfg#docfg()
    let l:localcfg_cfgs = localcfg#get_effective_list()

    let l:fn_array = s:create_fn_array()

    for k in keys(l:fn_array)
        if s:subset(l:fn_array[k], l:localcfg_cfgs)
            exe 'source' k
        endif
    endfor
endfun
